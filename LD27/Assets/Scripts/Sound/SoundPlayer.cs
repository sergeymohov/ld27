﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class SoundPlayer : Singleton<SoundPlayer> 
{
	[SerializeField]
	private GameObject soundObject;
	[SerializeField]
	private GameObject soundObject2D;
	
	[SerializeField]
	private List<AudioClip> soundFX = new List<AudioClip>();
	
	[SerializeField]
	private List<string> soundNames = new List<string>();
	
	private Dictionary<string, AudioClip> sounds = new Dictionary<string, AudioClip>();
	
	private bool mute3D = false;
	
	public bool Mute3D
	{
		get { return mute3D; }
		set { mute3D = value; }
	}
	
	private bool mute2D = false;
	public bool Mute2D
	{
		get { return mute2D; }
		set { mute2D = value; }
	}
	
	public AudioClip lengthReference;
	
	void Start () 
	{
		mute3D = false;

		//reassign stuff to the dictionary for easy access
		//doing this since can't serialize generic Dictionary
		try
		{
			for (int i = 0; i < soundFX.Count; i++)
			{
				sounds[soundNames[i]] = soundFX[i];
			}
		}
		catch
		{
			Debug.LogWarning("Some name or sound references missing in the inspector.");
		}
	}
	
	public void PlaySound2D(string effectName, bool randomizePitch = false)
	{
		GameObject soundGO = (GameObject)Instantiate(soundObject2D, Vector3.zero, Quaternion.identity);
		
		AudioSource soundSource = soundGO.audio;
		
		if (mute2D)
			soundSource.mute = true;
		
		soundGO.transform.parent = gameObject.transform;
		
		//current 2D sound volume in the project is 0.4
		soundSource.volume = 0.4f;
		
//		if (effectName == "Click_Unit")
//			soundSource.volume = 1f;
		
		if (randomizePitch)
			soundSource.pitch += Random.Range(-0.2f, 0.2f);
		
		soundSource.clip = (AudioClip)sounds[effectName];
		
		soundSource.Play();
	}
	
	public void PlaySound3D (string effectName, Vector3 soundPos, bool randomizePitch = false)
	{
		GameObject soundGO = (GameObject)Instantiate(soundObject, soundPos, Quaternion.identity);
		
		AudioSource soundSource = soundGO.audio;
		
		if (mute3D)
			soundSource.mute = true;
		
		soundGO.transform.parent = gameObject.transform;
		
		soundSource.volume = 2f;
		
		if (randomizePitch)
			soundSource.pitch += Random.Range(-0.5f, 0.5f);
		
		soundSource.maxDistance = 500f;
		
		if (effectName == "Footstep1" || effectName == "Footstep2")
		{
			soundSource.volume = 0.4f;
			soundSource.minDistance = 0.5f;
		}
		else if (effectName == "Collision")
		{
			soundSource.volume = 0.15f;
			soundSource.minDistance = 25f;
		}
		else if (effectName == "Wind")
		{
			soundSource.volume = 0.2f;
			soundSource.minDistance = 5f;
			soundSource.maxDistance = 50f;
		}
		else if (effectName == "Eat")
		{
			soundSource.volume = 0.5f;
			soundSource.minDistance = 25f;
		}
		else
		{
			soundSource.volume = 1f;
			soundSource.minDistance = 25f;
		}
		
		soundSource.clip = (AudioClip)sounds[effectName];
		
		soundSource.Play();
	}
	
	public GameObject PlaySound3DObj (string effectName, Vector3 soundPos, bool randomizePitch = false)
	{
		GameObject soundGO = (GameObject)Instantiate(soundObject, soundPos, Quaternion.identity);
		
		AudioSource soundSource = soundGO.audio;
		
		if (mute3D)
			soundSource.mute = true;
		
		soundGO.transform.parent = gameObject.transform;
		
		soundSource.volume = 2f;
		
		if (randomizePitch)
			soundSource.pitch += Random.Range(-0.5f, 0.5f);
		
		soundSource.maxDistance = 500f;
		
		if (effectName == "Footstep1" || effectName == "Footstep2")
		{
			soundSource.volume = 0.4f;
			soundSource.minDistance = 0.5f;
		}
		else if (effectName == "Collision")
		{
			soundSource.volume = 0.15f;
			soundSource.minDistance = 25f;
		}
		else if (effectName == "Wind")
		{
			soundSource.volume = 0.2f;
			soundSource.minDistance = 5f;
			soundSource.maxDistance = 50f;
		}
		else if (effectName == "Eat")
		{
			soundSource.volume = 0.4f;
			soundSource.minDistance = 25f;
		}
		else
		{
			soundSource.volume = 1f;
			soundSource.minDistance = 25f;
		}
		
		soundSource.clip = (AudioClip)sounds[effectName];
		
		soundSource.Play();
		
		return soundGO;
	}
}
