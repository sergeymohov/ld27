using UnityEngine;
using System.Collections;

public class SwitchStations : MonoBehaviour 
{
	[SerializeField]
	private AudioClip[] tracks = new AudioClip[0];
	private short nextTrack = 1;
	private float timeStore = 0f;
		
	void Start()
	{
		DontDestroyOnLoad(gameObject);
		RandomizeArray(tracks);
		audio.clip = tracks[0];
		audio.Play();
	}
	
	public void Reshuffle()
	{
		timeStore = audio.time;
		nextTrack = 1;
		RandomizeArray(tracks);
		audio.clip = tracks[0];
		
		if (timeStore >= audio.clip.length)
			timeStore = audio.clip.length / 2 + Random.Range(1f, 10f);
		
		audio.time = timeStore;
		audio.Play();
		timeStore = 0f;
	}
	
	void Update()
	{
		if (!audio.isPlaying)
		{
			audio.clip = tracks[nextTrack];
				
			nextTrack++;
			
			if (nextTrack > tracks.Length-1)
				nextTrack = 0;
			
			audio.time = 0;
			audio.Play();
		}
	}
	
	void RandomizeArray(AudioClip[] arr)
	{
	    for (int i = arr.Length - 1; i > 0; i--) 
		{
	        int r = Random.Range(0,i);
	        AudioClip tmp = arr[i];
	        arr[i] = arr[r];
	        arr[r] = tmp;
	    }
	}
	
}
