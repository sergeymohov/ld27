using UnityEngine;
using System.Collections;

public class DestroyAfterPlay : MonoBehaviour {

	void Update () 
	{
		if(audio && !audio.isPlaying)
		{
			Destroy(gameObject);
		}
	}
}
