﻿using UnityEngine;
using System.Collections;

public class SimpleMusicPlayer : Singleton<SimpleMusicPlayer>
{
	public AudioClip musicLoop;
	public AudioClip musicNoloop;
	private bool allowFading = false;
	
	void Start()
	{
		isPersistant = true;	
	}
	
	public void StartPlaying () 
	{
		if (!audio.isPlaying)
		{
			audio.Stop();
			audio.Play();
			Invoke("SwitchTrack", musicNoloop.length);
			allowFading = true;
		}
	}
	
	void Update()
	{
		
		if (allowFading)
		{
			if (GameController.instance.isIntro)
			{
				audio.volume = Mathf.Lerp(audio.volume, 0, 2 * Time.deltaTime);
				
				if (audio.volume <= 0.001)
				{
					CancelInvoke();
					audio.volume = 0;
					allowFading = false;
					audio.Stop();
					audio.loop = false;
					audio.clip = musicNoloop;
					audio.volume = 1;
				}
			}
			else
			{
				audio.volume = Mathf.Lerp(audio.volume, 1, 2 * Time.deltaTime);
				
				if (audio.volume >= 0.999)
				{
					audio.volume = 1;	
				}
			}
		}
	}
	
	void SwitchTrack()
	{
		audio.clip = musicLoop;
		
		if (!audio.isPlaying)
			audio.Play();
		
		audio.loop = true;	
	}
}
