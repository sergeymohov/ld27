﻿using UnityEngine;
using System.Collections;

public class LocalSpawner : MonoBehaviour {
	
	GameObject player;
	
	private Quaternion rotation1;
	
	private bool runUpdate = false;
	
	void RunCheck()
	{
		runUpdate = true;	
	}
	
	void Start () 
	{
		player = GameObject.Find("Character");
	}
	
	void Update () 
	{
		if (transform.position.x >= player.transform.position.x)
		{
			if (runUpdate && Vector3.Distance(player.transform.position, this.transform.position) < 520f)
			{
				//removes this cube and adds a ground instead, nice and clean
				GameController.instance.AddGround(transform.position);
			}
		}
		else
		{
			if (runUpdate && Vector3.Distance(player.transform.position, this.transform.position) < 260f)
			{
				//removes this cube and adds a ground instead, nice and clean
				GameController.instance.AddGround(transform.position);
			}
		}
	}
}
