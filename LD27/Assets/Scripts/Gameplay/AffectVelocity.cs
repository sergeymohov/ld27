﻿using UnityEngine;
using System.Collections;

public class AffectVelocity : MonoBehaviour 
{
	
	public float reduceVelocityBy = 10f;
	private Transform player;
	
	GameObject victim;
		
	void OnCollisionEnter (Collision other) 
	{
		if (other.collider.gameObject.name == "Character")
		{
			other.collider.gameObject.SendMessage("VelocityChange", new Vector3(-reduceVelocityBy, 0, -reduceVelocityBy));
			player = other.collider.transform;
			Replay();
		}
	}
	
	void OnCollisionExit(Collision other)
	{
		if (other.collider.gameObject.name == "Character")
		{
			CancelInvoke();
			if (victim) Destroy(victim);
			other.collider.gameObject.SendMessage("VelocityChange", new Vector3(reduceVelocityBy, 0, reduceVelocityBy));	
		}
	}
	
	void Replay()
	{
		victim = SoundPlayer.instance.PlaySound3DObj("Collision", player.position, true);
		Invoke("Replay", SoundPlayer.instance.lengthReference.length);
	}
}