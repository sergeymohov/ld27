﻿using UnityEngine;
using System.Collections;

public class CharController : MonoBehaviour 
{
	
	[SerializeField]
	private float speed = 1f;
	
	private float seconds = 10;
	
	public Vector3 resultingVelocity = Vector3.zero;
	
	public Vector3 additionalVelocity = Vector3.zero;
	
	private bool isCounting = false;
	
	private Transform object3D;
	
	private int rotors = 0;
	
	public int Rotors
	{
		get 
		{
			return rotors;
		}
		set
		{
			rotors = value;
		}
	}
	
	void Start()
	{
		object3D = transform.FindChild("CharacterMesh");	
		
		rotors = PlayerPrefs.GetInt("Rotors", 0);
	}
	
	public float Seconds
	{
		get
		{
			return seconds;
		}
		set
		{
			if (value < 0)
			{
				value = 0;
			
				GameController.instance.GameOver();
			}
			
			seconds = value;
		}
	}
	public bool showMessage1 = false;
	public bool showMessage2 = false;
	public GUIStyle newStyle;	
	public float plusHeight;
	public float plusWidth;
	public string messageText1 = string.Empty;
	public string messageText2 = string.Empty;
	void OnGUI()
	{
		if (showMessage1)
		{
			GUI.color = Color.black;
			GUI.Box(new Rect(Camera.main.WorldToScreenPoint(transform.position).x - 7, Camera.main.pixelHeight-Camera.main.WorldToScreenPoint(transform.position).y-25 + plusHeight + 5 +0, 200, 25), messageText1, newStyle);
			
			GUI.color = Color.white;
			GUI.Box(new Rect(Camera.main.WorldToScreenPoint(transform.position).x - 6, Camera.main.pixelHeight-Camera.main.WorldToScreenPoint(transform.position).y-25 + plusHeight + 6 +0, 200, 25), messageText1, newStyle);
			
		}
			
		if (showMessage2)
		{
			GUI.color = Color.black;
			GUI.Box(new Rect(Camera.main.WorldToScreenPoint(transform.position).x - 7, Camera.main.pixelHeight-Camera.main.WorldToScreenPoint(transform.position).y-25 + plusHeight + 5 +25, 200, 25), messageText2, newStyle);
			
			GUI.color = Color.white;
			GUI.Box(new Rect(Camera.main.WorldToScreenPoint(transform.position).x - 6, Camera.main.pixelHeight-Camera.main.WorldToScreenPoint(transform.position).y-25 + plusHeight + 6 +25, 200, 25), messageText2, newStyle);
		}
	
	}
	
	void VelocityChange (Vector3 change) 
	{
		additionalVelocity += change;
	}
	
	
	bool isLeft = false;
	IEnumerator StartCounting()
	{
		isCounting = true;
		
		iTween.PunchScale(object3D.gameObject, iTween.Hash("amount", new Vector3(0.1f, 0.1f, 0.1f), "time", 0.4f));
		
		GameObject clone = (GameObject)Resources.Load("Footstep");
		
		if (!isLeft)
		{
			isLeft = true;
			GameObject quad = (GameObject)Instantiate(clone, new Vector3(transform.position.x, clone.transform.position.y, transform.position.z + 1), clone.transform.rotation);
			quad.transform.eulerAngles = new Vector3(quad.transform.eulerAngles.x, Random.Range(-360, 361), quad.transform.eulerAngles.z);
			SoundPlayer.instance.PlaySound3D("Footstep1", quad.transform.position, true);
		}
		else
		{
			isLeft = false;
			GameObject quad = (GameObject)Instantiate(clone, new Vector3(transform.position.x, clone.transform.position.y, transform.position.z - 1), clone.transform.rotation);
			quad.transform.eulerAngles = new Vector3(quad.transform.eulerAngles.x, Random.Range(-360, 361), quad.transform.eulerAngles.z);
			SoundPlayer.instance.PlaySound3D("Footstep2", quad.transform.position, true);
		}
		
		yield return new WaitForSeconds(0.5f);
		
		StartCoroutine("StartCounting");
	}
	
	void StopCounting()
	{
		StopCoroutine("StartCounting");	
		isCounting = false;
	}
	
	Vector3 direction;
	void FixedUpdate () 
	{		
		direction = Vector3.zero;
		
		if (!GameController.instance.isIntro)
		{
			
			if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow))
			{			
				GameController.instance.gameStarted = true;
				direction += Vector3.right;
			}
			
			if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
			{			
				GameController.instance.gameStarted = true;
				direction += Vector3.left;
			}
			
			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
			{
				GameController.instance.gameStarted = true;
				direction += Vector3.forward;
			}
			
			if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
			{
				GameController.instance.gameStarted = true;
				direction += Vector3.back;
			}
			
			if (direction != Vector3.zero)
			{
				if (!isCounting)
					StartCoroutine("StartCounting");
			}
			else
			{
				StopCounting();	
			}
			
			object3D.forward = Vector3.Lerp(object3D.forward, direction, 4 * Time.fixedDeltaTime);
			
			Vector3 playerVelocity = (speed + additionalVelocity.x) * direction;
			rigidbody.MovePosition(rigidbody.position + playerVelocity * Time.deltaTime);		
			resultingVelocity = playerVelocity;
		}
		else
		{
			direction = Vector3.zero;
			if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
			{
				Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 70, 3 * Time.fixedDeltaTime);	
				direction += Vector3.forward;
			}
			else
			{
				Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 50, 1.5f * Time.fixedDeltaTime);		
			}
			
			Vector3 playerVelocity = (speed + additionalVelocity.x) * direction;
			rigidbody.MovePosition(rigidbody.position + playerVelocity * Time.deltaTime);		
			resultingVelocity = playerVelocity;
		}
	}
}
