﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class DestroyFar : MonoBehaviour {

	GameObject player;
	
	void Start () 
	{
		player = GameObject.Find("Character");
		
	}
	
	void Update () 
	{
		if (transform.position.x >= player.transform.position.x)
		{
			if (Vector3.Distance(player.transform.position, transform.position) > 650f)
			{
				GameController.instance.RemoveGround(transform.position);
			}
		}
		else
		{
			if (Vector3.Distance(player.transform.position, transform.position) > 300f)
			{
				GameController.instance.RemoveGround(transform.position);
			}
		}
		
	}
}
