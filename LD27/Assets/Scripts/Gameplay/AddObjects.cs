﻿using UnityEngine;
using System.Collections;

enum SpawnType
{
	tree,
	rock,
	fallenTree,
	windZone,
	swamp,
	wolf
}

public class AddObjects : MonoBehaviour 
{
	
	void OnEnable()
	{
		if (!GameController.instance.gameStarted)
		{
			//phase 0: trees and rocks
			Debug.Log("Phase 0");
			MakeTree(Random.Range(12, 20));
			MakeFallenTree (Random.Range(3, 6));
			MakeRock(Random.Range(6, 14));
		}
		else
		{
			//redefine time, start over
			//poor bastards will never get here anyhow
			if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 450)
			{
				Debug.Log("TIME REDEFINED, STARTING OVER");
				GameController.instance.gameTime = Time.timeSinceLevelLoad;
			}
			//phase 8
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 350)
			{
				Debug.Log("Phase 8");
				MakeTree(Random.Range(16, 75));
				MakeFallenTree (Random.Range(25, 45));
				MakeRock(Random.Range(55, 95));
				MakeFallenTreeBig(Random.Range(30, 35));
				MakeTreeBig(Random.Range(50, 66));
				MakeRotor(Random.Range(5, 18), 50f);
				MakeWindZone(Random.Range(16, 24));
			}
			//phase 7
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 250)
			{
				Debug.Log("Phase 7");
//				MakeTree(Random.Range(10, 45));
//				MakeFallenTree (Random.Range(1, 12));
				MakeRock(Random.Range(25, 45));
//				MakeFallenTreeBig(Random.Range(0, 7));
				MakeTreeBig(Random.Range(35, 66));
				MakeRotor(Random.Range(3, 10), 70f);
				MakeWindZone(Random.Range(4, 12));
			}
			//phase 6
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 180)
			{
				Debug.Log("Phase 6");
				MakeTree(Random.Range(10, 45));
				MakeFallenTree (Random.Range(1, 12));
				MakeRock(Random.Range(1, 10));
				MakeFallenTreeBig(Random.Range(0, 7));
				MakeTreeBig(Random.Range(15, 56));
				MakeRotor(Random.Range(2, 6), 60f);
				MakeWindZone(Random.Range(8, 16));
			}
			//phase 5
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 120)
			{
				Debug.Log("Phase 5");
				MakeTree(Random.Range(10, 25));
				MakeFallenTree (Random.Range(10, 20));
				MakeRock(Random.Range(50, 60));
				MakeFallenTreeBig(Random.Range(9, 20));
				MakeTreeBig(Random.Range(5, 15));
				MakeRotor(Random.Range(1, 5), 35f);
				MakeWindZone(Random.Range(5, 12));
			}
			//phase 4: hardcore begins
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 60)
			{
				Debug.Log("Phase 4");
				MakeTree(Random.Range(25, 30));
				MakeFallenTree (Random.Range(10, 20));
				MakeRock(Random.Range(30, 50));
				MakeFallenTreeBig(Random.Range(14, 25));
				MakeTreeBig(Random.Range(15, 25));
				MakeRotor(Random.Range(0, 4), 30f);
				MakeWindZone(Random.Range(1, 6));
			}
			
			//phase 3: even more trees and rocks
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 35)
			{
				Debug.Log("Phase 3");
				MakeTree(Random.Range(30, 35));
				MakeFallenTree (Random.Range(15, 25));
				MakeRock(Random.Range(30, 40));
				MakeFallenTreeBig(Random.Range(7, 11));
				MakeTreeBig(Random.Range(12, 14));
				MakeRotor(Random.Range(0, 3), 20f);
				MakeWindZone(Random.Range(0, 3));
			}
			
			//phase 2: more trees and rocks
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 10)
			{
				Debug.Log("Phase 2");
				MakeTree(Random.Range(20, 30));
				MakeFallenTree (Random.Range(6, 10));
				MakeRock(Random.Range(16, 24));
				MakeFallenTreeBig(Random.Range(3, 6));
				MakeTreeBig(Random.Range(4, 8));
				MakeRotor(Random.Range(0, 2), 15f);
				MakeWindZone(Random.Range(0, 1));
			}
			//phase 1: trees and rocks
			else if (Time.timeSinceLevelLoad - GameController.instance.gameTime > 0)
			{
				Debug.Log("Phase 1");
				MakeTree(Random.Range(12, 20));
				MakeFallenTree (Random.Range(3, 6));
				MakeRock(Random.Range(6, 14));
				MakeRotor(Random.Range(0, 1));
			}
		}
	}
	
	void MakeWindZone (int howMany)
	{
		for (int i = 0; i < howMany+1; i++)
		{
			GameObject clone = (GameObject)Resources.Load("WindZone");
			
			//instantiate trees, making sure that there's some space betweenthem by forcing int randomization
			GameObject tree = (GameObject)Instantiate(clone, new Vector3(Random.Range((int)Mathf.Round(transform.position.x - transform.lossyScale.x/2), (int)Mathf.Round(transform.position.x + transform.lossyScale.x/2) + 1), transform.position.y, 
																		 Random.Range((int)Mathf.Round(transform.position.z - transform.lossyScale.z/2), (int)Mathf.Round(transform.position.z + transform.lossyScale.z/2) + 1) ), clone.transform.rotation);
			
			tree.transform.eulerAngles = new Vector3(tree.transform.eulerAngles.x, Random.Range(-360, 361), tree.transform.eulerAngles.y);
			
			tree.transform.parent = transform;
		}
	}
	
	//chance in %
	void MakeRotor(int howMany, float chance = 10f)
	{
		for (int i = 0; i < howMany + 1; i++)
		{
			if (chance >= Random.Range(0, 101))
			{
				GameObject clone = (GameObject)Resources.Load("Shroom");
				
				//instantiate trees, making sure that there's some space betweenthem by forcing int randomization
				GameObject tree = (GameObject)Instantiate(clone, new Vector3(Random.Range((int)Mathf.Round(transform.position.x - transform.lossyScale.x/2), (int)Mathf.Round(transform.position.x + transform.lossyScale.x/2) + 1), transform.position.y, 
																			 Random.Range((int)Mathf.Round(transform.position.z - transform.lossyScale.z/2), (int)Mathf.Round(transform.position.z + transform.lossyScale.z/2) + 1) ), clone.transform.rotation);
				
				tree.transform.eulerAngles = new Vector3(tree.transform.eulerAngles.x, Random.Range(-360, 361), tree.transform.eulerAngles.y);
				
				tree.transform.parent = transform;
			}
		}
	}
	
	void MakeTree(int howMany)
	{
		for (int i = 0; i < howMany+1; i++)
		{
			GameObject clone = (GameObject)Resources.Load("Tree");
			
			//instantiate trees, making sure that there's some space betweenthem by forcing int randomization
			GameObject tree = (GameObject)Instantiate(clone, new Vector3(Random.Range((int)Mathf.Round(transform.position.x - transform.lossyScale.x/2), (int)Mathf.Round(transform.position.x + transform.lossyScale.x/2) + 1), transform.position.y, 
																		 Random.Range((int)Mathf.Round(transform.position.z - transform.lossyScale.z/2), (int)Mathf.Round(transform.position.z + transform.lossyScale.z/2) + 1) ), clone.transform.rotation);
			
			float addScale = Random.Range(-0.5f, 2.5f);
			tree.transform.localScale = new Vector3(tree.transform.localScale.x + addScale, tree.transform.localScale.y + addScale, tree.transform.localScale.z + addScale);
			tree.transform.eulerAngles = new Vector3(tree.transform.eulerAngles.x, Random.Range(-360, 361), tree.transform.eulerAngles.y);
			
			tree.transform.parent = transform;
		}
	}
	
	void MakeTreeBig(int howMany)
	{
		for (int i = 0; i < howMany+1; i++)
		{
			GameObject clone = (GameObject)Resources.Load("Tree");
			
			//instantiate trees, making sure that there's some space betweenthem by forcing int randomization
			GameObject tree = (GameObject)Instantiate(clone, new Vector3(Random.Range((int)Mathf.Round(transform.position.x - transform.lossyScale.x/2), (int)Mathf.Round(transform.position.x + transform.lossyScale.x/2) + 1), transform.position.y, 
																		 Random.Range((int)Mathf.Round(transform.position.z - transform.lossyScale.z/2), (int)Mathf.Round(transform.position.z + transform.lossyScale.z/2) + 1) ), clone.transform.rotation);
			
			float addScale = Random.Range(2.5f, 2.7f);
			tree.transform.localScale = new Vector3(tree.transform.localScale.x + addScale, tree.transform.localScale.y + addScale, tree.transform.localScale.z + addScale);
			tree.transform.eulerAngles = new Vector3(tree.transform.eulerAngles.x, Random.Range(-360, 361), tree.transform.eulerAngles.y);
			
			tree.transform.parent = transform;
		}
	}
	
	void MakeFallenTree(int howMany)
	{
		for (int i = 0; i < howMany+1; i++)
		{
			GameObject clone = (GameObject)Resources.Load("FallenTree");
			
			//instantiate trees, making sure that there's some space betweenthem by forcing int randomization
			GameObject tree = (GameObject)Instantiate(clone, new Vector3(Random.Range((int)Mathf.Round(transform.position.x - transform.lossyScale.x/2), (int)Mathf.Round(transform.position.x + transform.lossyScale.x/2) + 1), transform.position.y, 
																		 Random.Range((int)Mathf.Round(transform.position.z - transform.lossyScale.z/2), (int)Mathf.Round(transform.position.z + transform.lossyScale.z/2) + 1) ), clone.transform.rotation);
			
			float addScale = Random.Range(-0.5f, 2.5f);
			tree.transform.localScale = new Vector3(tree.transform.localScale.x + addScale, tree.transform.localScale.y + addScale, tree.transform.localScale.z + addScale);
			tree.transform.eulerAngles = new Vector3(Random.Range(-10, 11), Random.Range(-360, 361), Random.Range(-25, 26));
			
			tree.transform.parent = transform;
		}
	}
	void MakeFallenTreeBig(int howMany)
	{
		for (int i = 0; i < howMany + 1; i++)
		{
			GameObject clone = (GameObject)Resources.Load("FallenTree");
			
			//instantiate trees, making sure that there's some space betweenthem by forcing int randomization
			GameObject tree = (GameObject)Instantiate(clone, new Vector3(Random.Range((int)Mathf.Round(transform.position.x - transform.lossyScale.x/2), (int)Mathf.Round(transform.position.x + transform.lossyScale.x/2) + 1), transform.position.y, 
																		 Random.Range((int)Mathf.Round(transform.position.z - transform.lossyScale.z/2), (int)Mathf.Round(transform.position.z + transform.lossyScale.z/2) + 1) ), clone.transform.rotation);
			
			float addScale = Random.Range(2.5f, 2.7f);
			tree.transform.localScale = new Vector3(tree.transform.localScale.x + addScale, tree.transform.localScale.y + addScale, tree.transform.localScale.z + addScale);
			tree.transform.eulerAngles = new Vector3(Random.Range(-10, 11), Random.Range(-360, 361), Random.Range(-25, 26));
			
			tree.transform.parent = transform;
		}
	}
	
	void MakeRock(int howMany)
	{
		for (int i = 0; i < howMany+1; i++)
		{
			GameObject clone = (GameObject)Resources.Load("Rock");
			
			//instantiate rocks, making sure that there's some space betweenthem by forcing int randomization
			GameObject tree = (GameObject)Instantiate(clone, new Vector3(Random.Range((int)Mathf.Round(transform.position.x - transform.lossyScale.x/2), (int)Mathf.Round(transform.position.x + transform.lossyScale.x/2) + 1), transform.position.y, 
																		 Random.Range((int)Mathf.Round(transform.position.z - transform.lossyScale.z/2), (int)Mathf.Round(transform.position.z + transform.lossyScale.z/2) + 1) ), clone.transform.rotation);
			
			tree.transform.localScale = new Vector3(tree.transform.localScale.x + Random.Range(-0.5f, 2.5f), tree.transform.localScale.y + Random.Range(-0.5f, 2.5f), tree.transform.localScale.z + Random.Range(-0.5f, 1.5f));
			tree.transform.eulerAngles = new Vector3(Random.Range(-360, 361), Random.Range(-360, 361), Random.Range(-360, 361));
			
			tree.transform.parent = transform;
		}
	}
}
