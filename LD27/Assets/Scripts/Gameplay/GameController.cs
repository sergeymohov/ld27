﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class GameController : Singleton<GameController> 
{
	
	private Dictionary<int, string> dialogue = new Dictionary<int, string>();
	private Dictionary<int, string> dialogue2 = new Dictionary<int, string>();
	private Dictionary<int, string> currentStory;
	
	public GUIStyle style;
	public GUIStyle style2;
	public GUIStyle style3;
	public GUIStyle style4;
	public GUIStyle style5;
	public GUIStyle style6;
	
	public Color cold = new Color(4f/255f, 113f/255f, 167f/255f);
	public Color hot = new Color(167f/255f, 4f/255f, 4f/255f);
	public Color normalColor = Color.white;
	
	public bool isIntro = true;
	
	public float velocityMargin = 10f;
	
	public CharController player;
		
	GUISizer.GUIParams secondsLabel = new GUISizer.GUIParams(GUISizer.PositionDef.bottomMiddle, GUISizer.SizeDef.medium, "Frostbite in ");	
	GUISizer.GUIParams secondsLabel2 = new GUISizer.GUIParams(GUISizer.PositionDef.bottomMiddle, GUISizer.SizeDef.medium, "");
	GUISizer.GUIParams secondsLabel3 = new GUISizer.GUIParams(GUISizer.PositionDef.bottomMiddle, GUISizer.SizeDef.medium, "");
	GUISizer.GUIParams rotorsLabel = new GUISizer.GUIParams(GUISizer.PositionDef.bottomLeft, GUISizer.SizeDef.medium, "Rotors: ");	
	GUISizer.GUIParams homeLabel = new GUISizer.GUIParams(GUISizer.PositionDef.bottomRight, GUISizer.SizeDef.medium, "   Go home (H)");	
	GUISizer.GUIParams resetLabel = new GUISizer.GUIParams(GUISizer.PositionDef.bottomRight, GUISizer.SizeDef.medium, "Reset (R+P)");	
	
	GUISizer.GUIParams dialogueLabel = new GUISizer.GUIParams(GUISizer.PositionDef.topMiddle, GUISizer.SizeDef.medium, string.Empty);
	
	private bool timerRunning = false;
	
	public bool gameStarted = false;
	public float gameTime = 0f;
	
	public Dictionary<Vector3, GameObject> grounds = new Dictionary<Vector3, GameObject>();
	public Dictionary<Vector3, GameObject> cubes = new Dictionary<Vector3, GameObject>();
	
	[SerializeField]
	private Light mainLight;
	
	public void RemoveGround(Vector3 victimPos)
	{
		List <Vector3> groundsKeys = new List<Vector3>();
		
		foreach (KeyValuePair<Vector3, GameObject> entry in grounds)
		{
			groundsKeys.Add(entry.Key);	
		}
		
		List <Vector3> cubesKeys = new List<Vector3>();
		
		foreach (KeyValuePair<Vector3, GameObject> entry in cubes)
		{
			cubesKeys.Add(entry.Key);	
		}
		
		foreach(Vector3 key in groundsKeys)
		{
			if (Vector3.Distance(key, victimPos) < 10)
			{
				GameObject victimObj = grounds[key];
				grounds.Remove(key);
				
				foreach(Transform child in victimObj.transform)
				{
					foreach(Vector3 cubeKey in cubesKeys)
					{
						if (Vector3.Distance(cubeKey, child.position) < 10)
						{
							cubes.Remove(cubeKey);	
						}
					}
				}
				
				Destroy(victimObj);
			}
		}
		
		foreach (KeyValuePair<Vector3, GameObject> entry in grounds)
		{
			AddCubes(entry.Key);	
		}
		
	}
	
	public void AddGround(Vector3 cubePos)
	{
		bool groundsOK = true;
		bool cubesOK = false;
		
		foreach(KeyValuePair<Vector3, GameObject> entry in grounds)
		{
			if (Vector3.Distance(entry.Key, cubePos) < 10)
			{
				groundsOK = false;	
			}
		}
		
		foreach(KeyValuePair<Vector3, GameObject> entry in cubes)
		{
			if (Vector3.Distance(entry.Key, cubePos) < 10)
			{
				cubesOK = true;	
			}
		}
		
		if (cubesOK && groundsOK)
		{
			List <Vector3> cubesKeys = new List<Vector3>();
			
			foreach (KeyValuePair<Vector3, GameObject> entry in cubes)
			{
				cubesKeys.Add(entry.Key);	
			}
			
			foreach (Vector3 key in cubesKeys)
			{
				if (Vector3.Distance(key, cubePos) < 10)
				{
					GameObject cubeObj = cubes[key];
					cubes.Remove(key);
					
					Destroy(cubeObj);
					
					GameObject clone = (GameObject)Resources.Load("Ground");
					GameObject newGround = (GameObject)Instantiate(clone, key, clone.transform.rotation);
					grounds.Add(key, newGround);
					
					AddCubes(newGround.transform.position);
					
					newGround.AddComponent<AddObjects>();
				}
			}
		}
	}
	
	public void AddOneCube(Vector3 cubePos, Vector3 parentPos)
	{
		bool groundsOK = true;
		bool cubesOK = true;
		
		foreach(KeyValuePair<Vector3, GameObject> entry in grounds)
		{
			if (Vector3.Distance(entry.Key, cubePos) < 10)
			{
				groundsOK = false;
			}
		}
		
		foreach (KeyValuePair<Vector3, GameObject> entry in cubes)
		{
			if (Vector3.Distance(entry.Key, cubePos) < 10)
			{
				cubesOK = false;	
			}
		}
		
		if (groundsOK && cubesOK)
		{
			GameObject clone = (GameObject)Resources.Load("Cube");
			GameObject newCube = (GameObject)Instantiate(clone, cubePos, clone.transform.rotation);
			newCube.transform.parent = grounds[parentPos].transform;
			cubes.Add(cubePos, newCube);
			newCube.SendMessage("RunCheck");
		}
	}
	
	public void AddCubes(Vector3 aroundPos)
	{
		Vector3 left = new Vector3((int)Mathf.Round(aroundPos.x) - (int)Mathf.Round(grounds[aroundPos].transform.localScale.x), aroundPos.y, aroundPos.z);
		Vector3 right = new Vector3((int)Mathf.Round(aroundPos.x) + (int)Mathf.Round(grounds[aroundPos].transform.localScale.x), aroundPos.y, aroundPos.z);
		
		Vector3 top = new Vector3(aroundPos.x, aroundPos.y, (int)Mathf.Round(aroundPos.z) + (int)Mathf.Round(grounds[aroundPos].transform.localScale.z));
		Vector3 bottom = new Vector3(aroundPos.x, aroundPos.y, (int)Mathf.Round(aroundPos.z) - (int)Mathf.Round(grounds[aroundPos].transform.localScale.z));
		
		AddOneCube(left, aroundPos);
		AddOneCube(right, aroundPos);
		AddOneCube(top, aroundPos);
		AddOneCube(bottom, aroundPos);
	}
	
	void Start () 
	{
		dialogue[1] = "Welcome to Babushka VIII Pro storytelling system!\n\nInsert 1 Rotor to access Content (I)";
		dialogue[2] = "Once upon a time there was a world full of mystery and wizardry.\nUnfortunately, everybody died.\n\nInsert 2 Rotors to access Content (I)";
		dialogue[3] = "''Why did they die?'' you might ask.\nThe answer to this question is as simple as it is cruel.\n\nInsert 3 Rotors to access Content (I)";
		dialogue[4] = "They took many things for granted. Wizards and peasants alike, they recklessly disregarded the facts.\n\nInsert 4 Rotors to access Content (I)";
		dialogue[5] = "They listened to their leaders and didn't ask questions. They all died because of their collective failure to disenthrall themselves.\n\nInsert 5 Rotors to access Content (I)";
		dialogue[6] = "There was a wizard named Valerie. Valerie died as well, but let us forget about that for a minute.\n\nInsert 6 Rotors to access Content (I)";
		dialogue[7] = "People who weren't like Valerie hurt her very much when she was little and hadn't found her first Rotor yet.\n\nInsert 7 Rotors to access Content (I)";
		dialogue[8] = "Valerie was surrounded by people who weren't like her, and she assumed that they all were like the people who had hurt her.\n\nInsert 8 Rotors to access Content (I)";
		dialogue[9] = "Valerie took this for granted, and when she grew up, she wrote a book suggesting that these people would have to disappear.\n\nInsert 9 Rotors to access Content (I)";
		dialogue[10] = "Valerie's book was controversial, and many people believed in what she said.\n\nInsert 10 Rotors to access Content (I)";
		dialogue[11] = "Things that Valerie took for granted first became novel ideas, then popular ideas, then people started taking them for granted.\n\nInsert 11 Rotors to access Content (I)";
		dialogue[12] = "People who weren't like Valerie were systematically eradicated, even by the likes of themselves.\n\nInsert 12 Rotors to access Content (I)";
		dialogue[13] = "This came to be simply because everybody stopped questioning the truthfulness of an idea.\n\nInsert 13 Rotors to access Content (I)";
		dialogue[14] = "They forgot that people who weren't like Valerie had been necessary and, frankly, quite helpful.\n\nInsert 14 Rotors to access Content (I)";
		dialogue[15] = "The balance in society crumbled, the wizardry started to withdraw from the world. Buldings crashed, trees withered, population slowly but steadily decreased.\n\nInsert 15 Rotors to access Content (I)";
		dialogue[16] = "All that was left were Rotors, a constant reminder of a once great civilization that started taking things for granted.\n\nInsert 16 Rotors to access Content (I)";
		
		dialogue2[1] = "Welcome to Babushka VIII Pro storytelling system!\n\nInsert 1 Rotor to access Content (I)";
		dialogue2[2] = "No one knows for sure where Rotors came from.\nFortunately, they did.\n\nInsert 2 Rotors to access Content (I)";
		dialogue2[3] = "What everybody agrees upon is that Rotors came from elsewhere,\nand they are clearly not made using wizardry.\n\nInsert 3 Rotors to access Content (I)";
		dialogue2[4] = "As a matter of fact, Rotors got their name because of that:\nthey started spinning when wizardry wasn't around.\n\nInsert 4 Rotors to access Content (I)";
		dialogue2[5] = "Everybody thought that they protected themselves this way, but no one could tell for sure.\n\nInsert 5 Rotors to access Content (I)";
		dialogue2[6] = "Even though everybody tried to steal them, there was always the same quantity of Rotors in the world.\n\nInsert 6 Rotors to access Content (I)";
		dialogue2[7] = "Really quickly they began to serve as a currency for wizards and other folks.\n\nInsert 7 Rotors to access Content (I)";
		dialogue2[8] = "You could buy any wizard item using Rotors, you could also exchange them for information.\n\nInsert 8 Rotors to access Content (I)";
		dialogue2[9] = "At some point someone decided to make a better use of Rotors, and made a Stator.\n\nInsert 9 Rotors to access Content (I)";
		dialogue2[10] = "Stators were capable of producing power using Rotors. Since Rotors could spin indefinitely, power became cheap, and wizardry grew unpopular.\n\nInsert 10 Rotors to access Content (I)";
		dialogue2[11] = "It was then that the Wizard Wars, which ultimately led to the Depression, began. \n\nInsert 11 Rotors to access Content (I)";
		
		player = GameObject.Find("Character").GetComponent<CharController>();
		gameStarted = false;
		isIntro = true;
		lastTime = Time.time;
		gameTime = 0f;
		Camera.main.fieldOfView = 50;
	}
	
	void OnLevelWasLoaded()
	{
		int time = (int)Mathf.Round(PlayerPrefs.GetFloat("Seconds", 10f) * 1000);
		Debug.Log(time + " submitted");
		Application.ExternalCall("kongregate.stats.submit","Time", time);
	
	}
	
	public void GameOver()
	{
		int time = (int)Mathf.Round(PlayerPrefs.GetFloat("Seconds", 10f) * 1000);
		Debug.Log(time + " submitted " + player.Seconds);
		Application.ExternalCall("kongregate.stats.submit","Time", time);
		Application.LoadLevel("Scene1");	
	}
	
	private float lastTime;
	
	void FixedUpdate ()
	{
		if (gameStarted)
		{
			if (gameTime < 0.1f)
			{
				SimpleMusicPlayer.instance.StartPlaying();
				gameTime = Time.timeSinceLevelLoad;
			}
			
			if (!timerRunning && Mathf.Abs(player.resultingVelocity.x) <= velocityMargin && Mathf.Abs(player.resultingVelocity.z) <= velocityMargin)
			{
				Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 50, 1.5f * Time.fixedDeltaTime);
				player.Seconds -= (Time.time - lastTime);
			}
			else
			{
				Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 70, 3 * Time.fixedDeltaTime);	
			}
		}
		lastTime = Time.time;
		
		LightColor();
	}
	
	void LightColor()
	{
		mainLight.color = normalColor;
		
		if (player.Seconds >= 10f) // 20 is the max
		{
			float k = (player.Seconds-10f)/10f;
			mainLight.color = normalColor * (1F-k) + hot * k;
		}
		else 
		{
			float k2 = (player.Seconds/10f);
			k2 = k2*k2;
			mainLight.color = cold * (1F-k2) + normalColor * k2;
		}
	}
	
	bool keyDown = false;
	bool keyDown1 = false;
	void DisplayStory()
	{
		#if UNITY_EDITOR
		if (Input.GetMouseButtonUp(0))
			player.Rotors += 5;
		#endif
		
		
		
		if (PlayerPrefs.GetInt("DialogueDone", 0) == 0)
		{
			currentStory = dialogue;
		}
		else
		{
			currentStory = dialogue2;	
		}
		
		GUISizer.BeginGUI(GUISizer.PositionDef.topMiddle);
		
		GUISizer.MakeLabel(dialogueLabel, style5, currentStory[PlayerPrefs.GetInt("Story", 1)]);
		
		GUISizer.EndGUI();
		
		if (!keyDown && Input.GetKeyDown(KeyCode.I))
		{
			keyDown = true;
			if (player.Rotors >= PlayerPrefs.GetInt("Story", 1))
			{
				SoundPlayer.instance.PlaySound2D("Select", true);
				
				player.Rotors-= PlayerPrefs.GetInt("Story", 1);
				
				PlayerPrefs.SetInt("Rotors", player.Rotors);
				PlayerPrefs.SetInt("Story", PlayerPrefs.GetInt("Story", 1) + 1);
				
				if (PlayerPrefs.GetInt("Story", 1) > currentStory.Count)
				{
					PlayerPrefs.SetInt("Story", 1);
					if (currentStory == dialogue)
					{
						PlayerPrefs.SetInt("DialogueDone", 1);	
					}
					else
					{
						PlayerPrefs.SetInt("DialogueDone", 0);
					}
				}
			}
			else
			{
				SoundPlayer.instance.PlaySound2D("No", false);	
			}
		}
		
		if (keyDown && Input.GetKeyUp(KeyCode.I))
		{
			keyDown = false;
		}
	}
	
	void OnApplicationQuit()
	{
		PlayerPrefs.Save();	
		int time = (int)Mathf.Round(PlayerPrefs.GetFloat("Seconds", 10f) * 1000);
		Debug.Log(time + " submitted");
		Application.ExternalCall("kongregate.stats.submit","Time", time);
	}
	
	void OnGUI()
	{
		GUI.depth = -9999;
		
		GUISizer.BeginGUI(GUISizer.PositionDef.topLeft);
		GUISizer.MakeLabel(secondsLabel2, style, "");
		GUISizer.EndGUI();
	
		if (gameStarted)
		{
			GUISizer.BeginGUI(GUISizer.PositionDef.topLeft);
			if (player.Seconds < 1)
			{
				GUISizer.MakeLabel(secondsLabel2, style, "Frostbite imminent!");
			}
			else
			{
				GUISizer.MakeLabel(secondsLabel, style, ""+player.Seconds.ToString("F1") + " s.");
			}
			
			GUISizer.EndGUI();
			
			GUISizer.BeginGUI(GUISizer.PositionDef.bottomLeft);
			GUISizer.MakeLabel(rotorsLabel, style2, player.Rotors.ToString());
			GUISizer.EndGUI();
			
			GUISizer.BeginGUI(GUISizer.PositionDef.bottomRight);
			GUISizer.MakeLabel(homeLabel, style2);
			GUISizer.EndGUI();
			
			if (Input.GetKey(KeyCode.H))
			{
				SoundPlayer.instance.PlaySound2D("Select", true);
				
				int time = (int)Mathf.Round(PlayerPrefs.GetFloat("Seconds", 10f) * 1000);
				Debug.Log(time + " submitted");
				Application.ExternalCall("kongregate.stats.submit","Time", time);
				
				Application.LoadLevel(Application.loadedLevel);
			}
		}
		
		if (isIntro)
		{
			audio.mute = false;
			GUISizer.BeginGUI(GUISizer.PositionDef.topLeft);
			GUISizer.MakeLabel(secondsLabel2, style, "Record time: " + PlayerPrefs.GetFloat("Seconds", 10f).ToString("F1") + " s.");
			GUISizer.EndGUI();
			
			GUISizer.BeginGUI(GUISizer.PositionDef.bottomRight);
			GUISizer.MakeLabel(resetLabel, style4);
			GUISizer.MakeLabel(secondsLabel3, style6, "\nGo outside (Left arrow)");
			GUISizer.EndGUI();
			
			DisplayStory();
			
			if (!keyDown1 && Input.GetKey(KeyCode.R) && Input.GetKey(KeyCode.P))
			{
				keyDown1 = true;
				SoundPlayer.instance.PlaySound2D("Select", true);
				PlayerPrefs.DeleteAll();
				Application.LoadLevel(Application.loadedLevel);
			}
			if (keyDown1 &&( Input.GetKeyUp(KeyCode.R) || Input.GetKeyUp(KeyCode.P)))
			{
				keyDown1 = false;
			}
		}
		else
		{
			audio.mute = true;	
		}
		
		if (!gameStarted)
		{
			GUISizer.BeginGUI(GUISizer.PositionDef.bottomLeft);
			GUISizer.MakeLabel(rotorsLabel, style2, player.Rotors.ToString());
			GUISizer.EndGUI();
		}
	}
}