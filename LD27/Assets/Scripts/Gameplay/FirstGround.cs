﻿using UnityEngine;
using System.Collections;

public class FirstGround : MonoBehaviour {

	void Awake()
	{
		//for the very first ground
		GameController.instance.grounds.Add(transform.position, gameObject);
		
		foreach(Transform trans in transform)
		{
			GameController.instance.cubes.Add(trans.position, trans.gameObject);
			trans.gameObject.SendMessage("RunCheck");
		}
	}
}
