﻿using UnityEngine;
using System.Collections;

public class WindZoneOne : MonoBehaviour 
{
	GameObject victim;
	Transform player;
	void Start()
	{
		player = GameObject.Find("Character").transform;
	}
	
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.name == "Character")
		{
			if (victim) Destroy(victim);
			
			victim = SoundPlayer.instance.PlaySound3DObj("Wind", other.transform.position, true);
			victim.transform.parent = player;
			other.rigidbody.AddForce(transform.forward * 30, ForceMode.VelocityChange);
		}
	}
}
