﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour 
{
	public GUIStyle style;
	public GUIStyle style2;
	public GUIStyle style3;
	
	public GUIStyle style4;
	
	GUISizer.GUIParams titleLabel = new GUISizer.GUIParams(GUISizer.PositionDef.topMiddle, GUISizer.SizeDef.medium, string.Empty);
	GUISizer.GUIParams nameLabel = new GUISizer.GUIParams(GUISizer.PositionDef.topMiddle, GUISizer.SizeDef.medium, string.Empty);
	GUISizer.GUIParams hintLabel = new GUISizer.GUIParams(GUISizer.PositionDef.topMiddle, GUISizer.SizeDef.medium, string.Empty);
	
//	GUISizer.GUIParams ldLabel = new GUISizer.GUIParams(GUISizer.PositionDef.topRight, GUISizer.SizeDef.medium, string.Empty);
	
	private bool displayName = false;
	private bool displayHint = false;
	private bool run = false;
	
	private string hintText = string.Empty;
	
	void Start()
	{
		style.fontSize = 1;
	}
	float temp = 1f;
	void Update()
	{
		temp = Mathf.Lerp(temp, 110f, Time.time * 0.1f);
	}
	
	IEnumerator SwitchHint()
	{
		yield return new WaitForSeconds(0.4f);
		hintText = "(Enter) the game";
		yield return new WaitForSeconds(1.2f);
		hintText = string.Empty;
		StartCoroutine("SwitchHint");
	}
	
	IEnumerator DisplayRest()
	{
		run = true;
		yield return new WaitForSeconds(1);
		displayName = true;
		yield return new WaitForSeconds(2);
		displayHint = true;
		StartCoroutine("SwitchHint");
	}
	
	bool keyDown = false;
	void OnGUI () 
	{
		GUISizer.BeginGUI(GUISizer.PositionDef.topMiddle);
		
		style.fontSize = (int)Mathf.Round(temp);
		
		GUISizer.MakeLabel(titleLabel, style, "STATOR CHRONICLE");
		
		if (style.fontSize == 110)
		{
//			GUISizer.MakeLabel(ldLabel, style4, "LD27");
			
			if (!run)
				StartCoroutine("DisplayRest");
			
			if (displayName)
			{
				GUISizer.MakeLabel(nameLabel, style2, "The most painfully honest videogame by Sergey Mohov");
			}
			if (displayHint)
			{
				GUISizer.MakeLabel(hintLabel, style3, hintText);
				
				if (!keyDown && (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return)))
				{
					keyDown = true;
					SoundPlayer.instance.PlaySound2D("Select", true);
					StopCoroutine("SwitchHint");
					hintText = "The world is loading...";
					Application.LoadLevel("Scene1");
				}
				
				if (keyDown && (Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return)))
				{
					keyDown = false;
				}
			}
		}
		
		GUISizer.EndGUI();
	}
	
}
