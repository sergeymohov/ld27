﻿using UnityEngine;
using System.Collections;

public class ExitDoor : MonoBehaviour 
{
	public Collider[] houseBoxes;
	
	void OnTriggerEnter (Collider other) 
	{
		if (other.gameObject.name == "Character")
		{
			if (!houseBoxes[0].enabled)
			{
				houseBoxes[0].enabled = true;
				
				GameController.instance.isIntro = false;
			}
			else
			{
				Application.LoadLevel(Application.loadedLevel);	
			}
		}
	}
}
