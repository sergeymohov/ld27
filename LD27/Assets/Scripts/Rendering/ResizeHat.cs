﻿using UnityEngine;
using System.Collections;

public class ResizeHat : MonoBehaviour {
	
	void Update()
	{
		transform.Rotate(Vector3.up, Time.time);	
	}
	
	void Start () 
	{
		SoundPlayer.instance.PlaySound3D("RotorSpeech", transform.position, true);
		iTween.PunchScale(gameObject, iTween.Hash("amount", new Vector3(Random.Range(1f, 2f), Random.Range(1f, 2f), Random.Range(1f, 2f)), "time", Random.Range(0.5f, 1.5f), "oncomplete", "NewPunch"));
	}
	
	void NewPunch () 
	{
		SoundPlayer.instance.PlaySound3D("RotorSpeech", transform.position, true);
		iTween.PunchScale(gameObject, iTween.Hash("amount", new Vector3(Random.Range(1f, 2f), Random.Range(1f, 2f), Random.Range(1f, 2f)), "time", Random.Range(0.5f, 1.5f), "oncomplete", "OldPunch"));
	}
	
	void OldPunch()
	{
		SoundPlayer.instance.PlaySound3D("RotorSpeech", transform.position, true);
		iTween.PunchScale(gameObject, iTween.Hash("amount", new Vector3(Random.Range(1f, 2f), Random.Range(1f, 2f), Random.Range(1f, 2f)), "time", Random.Range(0.5f, 1.5f), "oncomplete", "NewPunch"));
	}
}
