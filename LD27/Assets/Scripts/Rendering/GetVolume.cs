﻿using UnityEngine;
using System.Collections;

public class GetVolume : MonoBehaviour 
{
	private float[] data  = new float[2048];
	float threshold = 0.25f;
	public bool change = false;
	private CharController charController;
	void Start()
	{
		charController = GameObject.Find("Character").GetComponent<CharController>();
	}
	
	void Update()
	{
		audio.GetOutputData(data,0);
		float sum = 0;
		foreach (float f in data)
		{
			sum += Mathf.Pow(f,2);
		}
		sum /=data.Length;
		sum = Mathf.Sqrt(sum);
		
		if (sum > threshold)
		{
			change = true;
		}
		else
		{
			change = false;
		}
	}
}
