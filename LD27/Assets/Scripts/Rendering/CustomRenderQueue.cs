using UnityEngine;
using System.Collections;

public class CustomRenderQueue : MonoBehaviour {

	[SerializeField]
	private int addRenderQueue = 0;
	// Use this for initialization
	void Start () 
	{
		renderer.material.renderQueue += addRenderQueue;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
